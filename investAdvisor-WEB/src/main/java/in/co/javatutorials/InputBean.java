package in.co.javatutorials;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.servlet.http.Part;

import tn.esprit.investAdvisor.managedBean.CurrencyBean;
import tn.esprit.investadvisor.contracts.Currency.IManagementCurrencyLocal;
import tn.esprit.investadvisor.entities.Currency;

@ManagedBean(name = "inputBean")
@ViewScoped
public class InputBean implements Serializable {

	@EJB
	IManagementCurrencyLocal currencyLocal;

	private static final long serialVersionUID = 9040359120893077422L;

	private Part part;
	private String statusMessage;
	private String name;
	private String code;
	private String logo;

	private Currency currency;

	public void doFindCurrency() {

		this.currency = currencyLocal.retrieveCurrency(currency.getId());
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String uploadFile() throws IOException {

		// Extract file name from content-disposition header of file part
		String fileName = getFileName(part);
		System.out.println("***** fileName: " + fileName);

		String basePath = "C:/Users/user/git/WEBCongo/investAdvisor-WEB/src/main/webapp/resources/images/";
		File outputFilePath = new File(basePath + fileName);

		// Copy uploaded file to destination path
		InputStream inputStream = null;
		OutputStream outputStream = null;
		try {
			inputStream = part.getInputStream();
			outputStream = new FileOutputStream(outputFilePath);

			int read = 0;
			final byte[] bytes = new byte[1024];
			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}

			statusMessage = "File upload successfull !!";
			Currency currency = new Currency();
			currency.setLogo("images/" + fileName);
			currency.setName(name);
			currency.setInd(code);
			currencyLocal.createCurrency(currency);
		} catch (IOException e) {
			e.printStackTrace();
			statusMessage = "File upload failed !!";
		} finally {
			if (outputStream != null) {
				outputStream.close();
			}
			if (inputStream != null) {
				inputStream.close();
			}
		}
		return null; // return to same page
	}

	public Part getPart() {
		return part;
	}

	public void setPart(Part part) {
		this.part = part;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	// Extract file name from content-disposition header of file part
	private String getFileName(Part part) {
		final String partHeader = part.getHeader("content-disposition");
		System.out.println("***** partHeader: " + partHeader);
		for (String content : part.getHeader("content-disposition").split(";")) {
			if (content.trim().startsWith("filename")) {
				return content.substring(content.indexOf('=') + 1).trim()
						.replace("\"", "");
			}
		}
		return null;
	}

	
}
