package tn.esprit.investAdvisor.managedBean;

import javax.ejb.EJB;

import javax.faces.bean.ManagedBean;


import tn.esprit.investadvisor.contracts.Newsletters.ManagementNewslettersLocal;
import tn.esprit.investadvisor.entities.Newsletters;


@javax.faces.bean.RequestScoped
@ManagedBean(name = "AddNewsLetters")
public class AddNewsLettersBean {

	private Newsletters newsletter = new Newsletters();

	private String name;
	private String URL;
	@EJB
	ManagementNewslettersLocal newsLettersService;
	public Newsletters getNewsletter() {
		return newsletter;
	}
	public void setNewsletter(Newsletters newsletter) {
		this.newsletter = newsletter;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	
	public String doCreateNewsLetter() {
		try {			
			
			newsLettersService.addNewsletters(newsletter);
			

			MessageUtil.addSuccessMessage("Post was successfully created.");
			return "/public/AfficheNewsLetters?faces-redirect=true" ;
		} catch (Exception e) {
			MessageUtil.addErrorMessage("Post could not be saved. A Persisting error occured.");
		}

		return null;
	}
	public void clearNewsLetter() {
		this.newsletter = new Newsletters();
	}

}
