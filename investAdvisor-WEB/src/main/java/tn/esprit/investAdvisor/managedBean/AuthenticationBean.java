package tn.esprit.investAdvisor.managedBean;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import tn.esprit.investadvisor.contracts.Client.IManagementClientLocal;
import tn.esprit.investadvisor.contracts.admin.IManagementAdminLocal;
import tn.esprit.investadvisor.entities.Admin;
import tn.esprit.investadvisor.entities.Client;




@SessionScoped
@ManagedBean(name="auth")
public class AuthenticationBean {
	 
	@EJB
	IManagementAdminLocal adminMAdminLocal;
	@EJB
	IManagementClientLocal ciClientLocal;
	
	
	
	
	

	private String login;
	private String password;
	private Admin admin;
	
	private List<Client> nonvalidClients = new ArrayList<Client>();
	private List<Client> activateClients = new ArrayList<Client>();
	private List<Client> deactivateClients = new ArrayList<Client>();

	private String confPass;

	public String getConfPass() {
		return confPass;
	}

	public void setConfPass(String confPass) {
		this.confPass = confPass;
	}

	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Admin getAdmin() {
		return admin;
	}
	public void setAdmin(Admin admin) {
		this.admin = admin;
	}
	
	
	public String doAuthenticate() {
		try{
			admin = adminMAdminLocal.findAdminByLogin(login);
					
			if (admin.getPassword().equals(password)) {
				
				//doMorisStat();
				//System.out.println("conn"+conectedClient);
				//System.out.println(disconnnectedClient);
				return "/public/index2?faces-redirect=true";
			}else 
		
			FacesContext.getCurrentInstance().addMessage(
					"loginFormId:loginSubmitId",
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Bad Password", "Bad Credentials"));
			return null;
		}
		catch(Exception e){
			
			FacesContext.getCurrentInstance().addMessage(
					"loginFormId:loginSubmitId",
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Admin not Found", "Admin Not Found"));
			return null;}
	}
	
	public void doAllClient() {
		try {

			deactivateClients = ciClientLocal.findClientByState(1);
			nonvalidClients = ciClientLocal.findClientByState(0);
			activateClients = ciClientLocal.findClientByState(2);
			

		} catch (Exception e) {

			System.out.println(e.getMessage());
		}
	}

	public List<Client> getNonvalidClients() {
		return nonvalidClients;
	}

	public void setNonvalidClients(List<Client> nonvalidClients) {
		this.nonvalidClients = nonvalidClients;
	}

	public List<Client> getActivateClients() {
		return activateClients;
	}

	public void setActivateClients(List<Client> activateClients) {
		this.activateClients = activateClients;
	}

	public List<Client> getDeactivateClients() {
		return deactivateClients;
	}

	public void setDeactivateClients(List<Client> deactivateClients) {
		this.deactivateClients = deactivateClients;
	}

	public String doActivateClient(Integer id) {
		Client client=ciClientLocal.findClientById(id);
		client.setValid(2);
		ciClientLocal.updateClient(client);
		return "/public/client";
		
	}
	public String doDeActivateClient(Integer id) {
		Client client;
		try {
			client = ciClientLocal.findClientById(id);
			client.setValid(1);
			ciClientLocal.updateClient(client);
			System.out.println(id);
			return "/public/client";
		} catch (Exception e) {
			System.out.println("ok"+id);
			e.printStackTrace();
			return null;			
		}
		
	}
	

	public void findAdmin() {
		this.admin = adminMAdminLocal.findAdminById(this.admin.getId());
	}

	public String doUpdateAdmin() {
		if (admin.getPassword().equals(confPass)) {

			adminMAdminLocal.updateAdmin(admin);

			return "/public/index2?faces-redirect=true";

		}
		return null;
	}
}
