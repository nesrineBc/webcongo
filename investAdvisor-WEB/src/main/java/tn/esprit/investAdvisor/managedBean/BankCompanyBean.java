package tn.esprit.investAdvisor.managedBean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import tn.esprit.investadvisor.contracts.BankCompany.IManagementBankCompanyLocal;
import tn.esprit.investadvisor.contracts.Company.IManagementCompanyLocal;
import tn.esprit.investadvisor.contracts.NonBankCompany.IManagementNonBankCompanyLocal;
import tn.esprit.investadvisor.entities.BankCompany;
import tn.esprit.investadvisor.entities.NonBankCompany;

@SessionScoped
@ManagedBean(name = "Bank")
public class BankCompanyBean {
	@EJB
	IManagementCompanyLocal companyService;

	@EJB
	IManagementBankCompanyLocal bankcompanyService;
	@EJB
	IManagementNonBankCompanyLocal nonBankCompanyService;

	List<NonBankCompany> Nonbankcompanies = new ArrayList<NonBankCompany>();
	List<BankCompany> bankcompanies = new ArrayList<BankCompany>();

	private boolean VisibleBank;
	private boolean VisibleNonBank;
	
	
	@PostConstruct
	public void init() {
		setBankcompanies(doImportBankCompany());
		setNonbankcompanies(doImportNonBankCompany());
		setVisibleBank(false);
		setVisibleNonBank(true);
	}

	public boolean isVisibleBank() {
		return VisibleBank;
	}

	public void setVisibleBank(boolean visibleBank) {
		VisibleBank = visibleBank;
	}

	public boolean isVisibleNonBank() {
		return VisibleNonBank;
	}

	public void setVisibleNonBank(boolean visibleNonBank) {
		VisibleNonBank = visibleNonBank;
	}

	public List<NonBankCompany> getNonbankcompanies() {
		return Nonbankcompanies;
	}

	public void setNonbankcompanies(List<NonBankCompany> nonbankcompanies) {
		Nonbankcompanies = nonbankcompanies;
	}

	public List<BankCompany> getBankcompanies() {
		return bankcompanies;
	}

	public void setBankcompanies(List<BankCompany> bankcompanies) {
		this.bankcompanies = bankcompanies;
	}

	public List<BankCompany> doImportBankCompany() {
		return bankcompanyService.findAll();
	}

	public List<NonBankCompany> doImportNonBankCompany() {
		return nonBankCompanyService.findAll();
	}

	public void doShowBanks() {
		setVisibleBank(true);
		setVisibleNonBank(false);
	}

	public void doShowNonBank() {
		setVisibleBank(false);
		setVisibleNonBank(true);
	}

	public void doShowAll() {
		setVisibleBank(true);
		setVisibleNonBank(true);
	}

	
}
