package tn.esprit.investAdvisor.managedBean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

import tn.esprit.investadvisor.contracts.Claims.ManagementClaimsRemote;
import tn.esprit.investadvisor.entities.Claims;



@RequestScoped
@ManagedBean(name="claims")
public class ClaimsBean {

	
	
	@EJB
	ManagementClaimsRemote claimsRemote;
	
	
	private List<Claims> claims=new ArrayList<Claims>();

	
	
	@PostConstruct
	public void init(){
		doAllClaims();
	}
	public List<Claims> getClaims() {
		return claims;
	}

	public void setClaims(List<Claims> claims) {
		this.claims = claims;
	}
	
	
	public void doAllClaims(){
		claims=claimsRemote.findAllClaims();
	}
	
	
	}
