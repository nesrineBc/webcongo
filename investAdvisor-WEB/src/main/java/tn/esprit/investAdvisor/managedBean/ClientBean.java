package tn.esprit.investAdvisor.managedBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

import tn.esprit.investadvisor.contracts.Client.IManagementClientLocal;
import tn.esprit.investadvisor.contracts.wallet.IManagementWalletLocal;
import tn.esprit.investadvisor.entities.Client;
import tn.esprit.investadvisor.entities.Wallet;

@RequestScoped
@ManagedBean(name = "client")
public class ClientBean {

	@EJB
	IManagementClientLocal ciClientLocal;

	@EJB
	IManagementWalletLocal iManagementWalletLocal;
	private List<Client> decc = new ArrayList<Client>();
	private List<Integer> aaa = new ArrayList<Integer>();
	
	private List<Client> allClients = new ArrayList<Client>();
	private List<Client> clients = new ArrayList<Client>();

	public List<Client> getClients() {
		return clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	private Integer conectedClient;
	private Integer disconnnectedClient;

	public Integer getConectedClient() {
		return conectedClient;
	}

	public void setConectedClient(Integer conectedClient) {
		this.conectedClient = conectedClient;
	}

	public Integer getDisconnnectedClient() {
		return disconnnectedClient;
	}

	public void setDisconnnectedClient(Integer disconnnectedClient) {
		this.disconnnectedClient = disconnnectedClient;
	}

	public void doMorisStat() {
		try {
			List<Client> clientsConnectedNow = ciClientLocal
					.findClientByLastConnectionDate();
			List<Client> clientsNotConnectedNow = ciClientLocal
					.findClientByLastConnectionDateNotNull();
			conectedClient = clientsConnectedNow.size();
			disconnnectedClient = clientsNotConnectedNow.size();
			System.out.println("connectedClient" + conectedClient);
			System.out.println("disconnected clients" + disconnnectedClient);

		} catch (Exception e) {

			System.out.println(e.getMessage());
		}
	}
	
	@PostConstruct
	public void init(){
		allClients = ciClientLocal.findAllClient();
	}

	public void doBestClient() {
		try {
			List<Wallet> clientsConnectedNow = iManagementWalletLocal.findAll();
			Collections.sort(clientsConnectedNow, Collections.reverseOrder());
			Float max = clientsConnectedNow.get(0).getWalletValue();
			for (int i = 0; i < 5; i++) {

				Integer resultat;
				resultat = (int) (clientsConnectedNow.get(i).getWalletValue() * 100 / max);
				clientsConnectedNow.get(i).getClient().setAge(resultat);
				clients.add(clientsConnectedNow.get(i).getClient());

			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void doWorstClient() {
		try {
			List<Wallet> clientsConnectedNow = iManagementWalletLocal.findAll();

			Collections.sort(clientsConnectedNow);
			Float max2 = clientsConnectedNow.get(4).getWalletValue();
			for (int i = 0; i < 5; i++) {

				Integer resultat;
				resultat = (int) (clientsConnectedNow.get(i).getWalletValue() * 100 / max2);
				clientsConnectedNow.get(i).getClient().setAge(resultat);
				decc.add(clientsConnectedNow.get(i).getClient());
			}

		} catch (Exception e) {

			System.out.println(e.getMessage());
		}
	}

	public List<Client> getDecc() {
		return decc;
	}

	public void set(List<Client> decc) {
		this.decc = decc;
	}

	public List<Integer> getAaa() {
		return aaa;
	}

	public void setAaa(List<Integer> aaa) {
		this.aaa = aaa;
	}

	

	public List<Client> getAllClients() {
		return allClients;
	}

	public void setAllClients(List<Client> allClients) {
		this.allClients = allClients;
	}
	

}
