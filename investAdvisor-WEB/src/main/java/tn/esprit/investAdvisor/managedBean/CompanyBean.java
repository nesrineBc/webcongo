package tn.esprit.investAdvisor.managedBean;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import tn.esprit.investadvisor.contracts.BankCompany.IManagementBankCompanyLocal;
import tn.esprit.investadvisor.contracts.Company.IManagementCompanyLocal;
import tn.esprit.investadvisor.contracts.NonBankCompany.IManagementNonBankCompanyLocal;
import tn.esprit.investadvisor.contracts.Sector.IManagementSectorLocal;
import tn.esprit.investadvisor.entities.BankCompany;
import tn.esprit.investadvisor.entities.Company;
import tn.esprit.investadvisor.entities.NonBankCompany;
import tn.esprit.investadvisor.entities.Sector;

@RequestScoped
@ManagedBean(name = "company")
public class CompanyBean {

	private Integer componyNumber;
	private Integer bankcomponyNumber;
	private Integer noncomponyNumber;
	private Integer sectorNumber;

	@EJB
	IManagementCompanyLocal companyService;
	@EJB
	IManagementBankCompanyLocal bankcompanyService;
	@EJB
	IManagementNonBankCompanyLocal noncompanyService;
	@EJB
	IManagementSectorLocal sectorservices;
	public Integer getComponyNumber() {
		return componyNumber;
	}

	public void setComponyNumber(Integer componyNumber) {
		this.componyNumber = componyNumber;
	}

	public void doListAllCompany(){
		List<Company> companies=companyService.findAllCompany();
		List<BankCompany> bankCompanies=bankcompanyService.findAll();
		List<NonBankCompany> nonBankCompanies=noncompanyService.findAll();
		bankcomponyNumber=bankCompanies.size();
		noncomponyNumber=nonBankCompanies.size();
		componyNumber=companies.size();
		List<Sector> setors=sectorservices.retrieveAllSector();
		sectorNumber=setors.size();
	}

	public Integer getBankcomponyNumber() {
		return bankcomponyNumber;
	}

	public void setBankcomponyNumber(Integer bankcomponyNumber) {
		this.bankcomponyNumber = bankcomponyNumber;
	}

	public Integer getNoncomponyNumber() {
		return noncomponyNumber;
	}

	public void setNoncomponyNumber(Integer noncomponyNumber) {
		this.noncomponyNumber = noncomponyNumber;
	}

	public Integer getSectorNumber() {
		return sectorNumber;
	}

	public void setSectorNumber(Integer sectorNumber) {
		this.sectorNumber = sectorNumber;
	}
	
}
