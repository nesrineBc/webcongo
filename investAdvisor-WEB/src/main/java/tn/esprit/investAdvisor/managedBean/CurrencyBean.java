package tn.esprit.investAdvisor.managedBean;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


import javax.servlet.http.Part;

import tn.esprit.investadvisor.contracts.Currency.IManagementCurrencyLocal;
import tn.esprit.investadvisor.entities.Currency;

@SessionScoped
@ManagedBean(name = "Currency")
public class CurrencyBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Currency> currencyList = new ArrayList<Currency>();
	private Currency currency = new Currency();
	private Integer id;
	private String name;
	private String logo;
	private Part part;
	private String code;
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	private String statusMessage;
	
	public Part getPart() {
		return part;
	}

	public void setPart(Part part) {
		this.part = part;
	}

	@EJB
	IManagementCurrencyLocal currencyService;

	public List<Currency> getCurrencyList() {
		return currencyList;
	}

	public void setCurrencyList(List<Currency> currencyList) {
		this.currencyList = currencyList;
	}

	

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String doUpdateCurrency() {
		try {
						
			currencyService.updateCurrency(currency);

			MessageUtil.addSuccessMessage("Post was successfully created."
					+ this.currency.getId());
		} catch (Exception e) {
			MessageUtil
					.addErrorMessage("Post could not be saved. A Persisting error occured."
							+ this.currency.getId());
		}

		return null;
	}

	public void doLoadCurrenies() {
		this.currencyList =currencyService.retrieveAllCurrency() ;
	}

	public void doFindCurrency() {

		
		this.currency=currencyService.retrieveCurrency(currency.getId()) ;
	}

	public String doDeleteCurrency(Currency currency) {

		try {

			currencyService.deleteCurrency(currency);
			MessageUtil.addSuccessMessage("Post was successfully deleted.");
		} catch (Exception e) {
			MessageUtil.addErrorMessage("Could not delete currency.");
		}

		return null;
	}
	
	public String uploadFile() throws IOException {

		// Extract file name from content-disposition header of file part
		String fileName = getFileName(part);
		System.out.println("***** fileName: " + fileName);

		String basePath = "C:/Users/user/git/WEBCongo/investAdvisor-WEB/src/main/webapp/resources/images/";
		File outputFilePath = new File(basePath + fileName);

		// Copy uploaded file to destination path
		InputStream inputStream = null;
		OutputStream outputStream = null;
		try {
			inputStream = part.getInputStream();
			outputStream = new FileOutputStream(outputFilePath);

			int read = 0;
			final byte[] bytes = new byte[1024];
			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}

			statusMessage = "File upload successfull !!";
			currency.setLogo("images/" + fileName);
			doUpdateCurrency();
		} catch (IOException e) {
			e.printStackTrace();
			statusMessage = "File upload failed !!";
		} finally {
			if (outputStream != null) {
				outputStream.close();
			}
			if (inputStream != null) {
				inputStream.close();
			}
		}
		return null; // return to same page
	}
	
	private String getFileName(Part part) {
		final String partHeader = part.getHeader("content-disposition");
		System.out.println("***** partHeader: " + partHeader);
		for (String content : part.getHeader("content-disposition").split(";")) {
			if (content.trim().startsWith("filename")) {
				return content.substring(content.indexOf('=') + 1).trim()
						.replace("\"", "");
			}
		}
		return null;
	}

}
