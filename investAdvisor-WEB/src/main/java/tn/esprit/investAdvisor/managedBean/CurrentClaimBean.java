package tn.esprit.investAdvisor.managedBean;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import tn.esprit.investadvisor.contracts.Claims.ManagementClaimsLocal;
import tn.esprit.investadvisor.contracts.Claims.ManagementClaimsRemote;
import tn.esprit.investadvisor.contracts.Client.IManagementClientLocal;
import tn.esprit.investadvisor.entities.Claims;



@SessionScoped
@ManagedBean(name="curclaim")
public class CurrentClaimBean {

	@EJB
	ManagementClaimsLocal claimsLocal;
	
	private Claims claims=new Claims();

	public Claims getClaims() {
		return claims;
	}

	public void setClaims(Claims claims) {
		this.claims = claims;
	}
	
	public void findClaim(){
		
		this.claims=claimsLocal.findClaimsById(this.claims.getId());
	}
}
