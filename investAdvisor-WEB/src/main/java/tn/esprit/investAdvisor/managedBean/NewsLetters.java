package tn.esprit.investAdvisor.managedBean;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;




import tn.esprit.investadvisor.contracts.Newsletters.ManagementNewslettersLocal;

import tn.esprit.investadvisor.entities.Newsletters;


@SessionScoped
@ManagedBean(name = "NewsLetters")
public class NewsLetters {

	private List<Newsletters> newsLettersList = new ArrayList<Newsletters>();
	private Newsletters newsletter = new Newsletters();
	private Integer id;
	private String name;
	private String URL;
	@EJB
	ManagementNewslettersLocal newsLettersService;
	public List<Newsletters> getNewsLettersList() {
		return newsLettersList;
	}
	public void setNewsLettersList(List<Newsletters> newsLettersList) {
		this.newsLettersList = newsLettersList;
	}
	public Newsletters getNewsLetters() {
		return newsletter;
	}
	public void setNewsLetters(Newsletters newsLetters) {
		this.newsletter = newsLetters;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	
	
	public String doUpdateNewsletter() {
		try {
						
			
			newsLettersService.updateNewletters(newsletter);

			MessageUtil.addSuccessMessage("Post was successfully created."
					+ this.newsletter.getId());
		} catch (Exception e) {
			MessageUtil
					.addErrorMessage("Post could not be saved. A Persisting error occured."
							+ this.newsletter.getId());
		}

		return null;
	}

	public void doLoadNewsLetters() {
		
		this.newsLettersList=newsLettersService.findAllNewsletters();
		
	}

	public void doFindNewsLetters() {

		
		
		this.newsletter=newsLettersService.findNewsletterById(newsletter.getId()) ;
		
	}

	public String doDeleteNewsLetter(Newsletters newsletters) {

		try {

			
			newsLettersService.deleteClaims(newsletters);
		//	MessageUtil.addSuccessMessage("Post was successfully deleted.");
		} catch (Exception e) {
		//	MessageUtil.addErrorMessage("Could not delete news letter.");
		}

		return null;
	}

	
}
