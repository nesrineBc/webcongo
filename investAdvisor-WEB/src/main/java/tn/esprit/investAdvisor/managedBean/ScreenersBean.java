package tn.esprit.investAdvisor.managedBean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import tn.esprit.investadvisor.contracts.Screeners.IManagementScreenersLocal;
import tn.esprit.investadvisor.entities.Screener;




@SessionScoped
@ManagedBean(name="screeners")
public class ScreenersBean {

	
	
	@EJB
	IManagementScreenersLocal screenersLocal;
	
	
	List<Screener> screeners=new ArrayList<Screener>();
	@PostConstruct
	public void init(){
		screeners=screenersLocal.findAllScreeners();
	}
	public List<Screener> getScreeners() {
		return screeners;
	}
	public void setScreeners(List<Screener> screeners) {
		this.screeners = screeners;

	}
	
	public void doUpdateScreeners(Integer i){
		
		Screener screener=screeners.get(i);
		screener.setActive((screener.getActive()+1)%2);
		screenersLocal.updateScreener(screener);
	}
}

