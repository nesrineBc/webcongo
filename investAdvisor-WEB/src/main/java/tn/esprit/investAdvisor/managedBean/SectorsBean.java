package tn.esprit.investAdvisor.managedBean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import tn.esprit.investadvisor.contracts.Sector.IManagementSectorLocal;
import tn.esprit.investadvisor.entities.Sector;

@SessionScoped
@ManagedBean(name="Sector")
public class SectorsBean {
	
	List<Sector> Sectors= new ArrayList<Sector>();
	
	@EJB
	IManagementSectorLocal SectorService;
	@PostConstruct
	public void init(){
		setSectors(doListSector());
	}

	public List<Sector> getSectors() {
		return Sectors;
	}

	public void setSectors(List<Sector> sectors) {
		Sectors = sectors;
	}

	public List<Sector> doListSector(){
		return SectorService.retrieveAllSector();
	}
}
