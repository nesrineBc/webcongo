package tn.esprit.investAdvisor.managedBean;

import java.rmi.server.Operation;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.webapp.ValidatorTag;

import tn.esprit.investadvisor.contracts.Client.IManagementClientLocal;
import tn.esprit.investadvisor.contracts.Operation.IManagementOperationLocal;
import tn.esprit.investadvisor.entities.Client;

@ManagedBean
@RequestScoped
public class SingelClientManagedBean {
private Client client;
private int id;
private String validity;
private float walletValue;
private List<tn.esprit.investadvisor.entities.Operation> operations;
Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();


String idS = params.get("clientId");

public List<tn.esprit.investadvisor.entities.Operation> getOperations() {
	return operations;
}
public void setOperations(
		List<tn.esprit.investadvisor.entities.Operation> operations) {
	this.operations = operations;
}
public String getValidity() {
	return validity;
}
public float getWalletValue() {
	return walletValue;
}
public void setWalletValue(float walletValue) {
	this.walletValue = walletValue;
}
public void setValidity(String validity) {
	this.validity = validity;
}
@EJB
private IManagementClientLocal ciClientLocal; 
@EJB
private IManagementOperationLocal ciOperationLocal;
@PostConstruct
public void init(){
	id= Integer.valueOf(idS);
	client= ciClientLocal.findClientById(id);
	if(client.getValid()==0)
		validity="Account Not Activated";
	else if(client.getValid()==1)
		validity="Simulation disabled";
	else {		
		validity="Simulation enabled";
		walletValue=client.getWallet().getWalletValue();
		operations=ciOperationLocal.findOperationByClient(client);
	}
	}
public Client getClient() {
	return client;
}

public void setClient(Client client) {
	this.client = client;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public IManagementClientLocal getCiClientLocal() {
	return ciClientLocal;
}
public void setCiClientLocal(IManagementClientLocal ciClientLocal) {
	this.ciClientLocal = ciClientLocal;
}

}
