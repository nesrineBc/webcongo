package tn.esprit.investAdvisor.managedBean;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import tn.esprit.investadvisor.contracts.Client.IManagementClientLocal;
import tn.esprit.investadvisor.entities.Client;

 
@ManagedBean(name="user")
@RequestScoped
public class UserBean {
 
	
	public String[] favFood3;
	@EJB
	IManagementClientLocal ciClientLocal;
 
	//getter and setter methods...
 
	
 
	public String getFavFood3() {
		return Arrays.toString(favFood3);
	}
 
	//Generated by Map
	private static Map<String,Object> food2Value;
	static{
		food2Value = new LinkedHashMap<String,Object>();
		food2Value.put("Food2 - Fry Checken", "Fry Checken"); //label, value
		food2Value.put("Food2 - Tomyam Soup", "Tomyam Soup");
		food2Value.put("Food2 - Mixed Rice", "Mixed Rice");
	}
 
	public Map<String,Object> getFavFood2Value() {
		return food2Value;
	}
 
	//Generated by Object array
	public static class Food{
		public Integer foodLabel;
		public String foodValue;
 
		public Food(Integer foodLabel, String foodValue){
			this.foodLabel = foodLabel;
			this.foodValue = foodValue;
		}
 
		public Integer getFoodLabel(){
			return foodLabel;
		}
 
		public String getFoodValue(){
			return foodValue;
		}
 
	}
 
	public Food[] food3List;
 
	public Food[] getFavFood3Value() {
 
		List<Client> clients = ciClientLocal.findAllClient();
		Map<String, Integer> clientsmap = new TreeMap<String, Integer>();
		for (int i = 0; i < clients.size(); i++) {
			if (clientsmap.keySet().contains(
					clients.get(i).getRegistrationDate().toString()))
				clientsmap.put(
						clients.get(i).getRegistrationDate().toString(),
						clientsmap.get(clients.get(i).getRegistrationDate()
								.toString()) + 1);
			else
				clientsmap.put(clients.get(i).getRegistrationDate().toString(),
						1);

		}
		food3List = new Food[clientsmap.size()];
		int i=0;
		for(Entry<String, Integer> entry : clientsmap.entrySet()) {
			
		    
		    food3List[i] = new Food(entry.getValue(),entry.getKey());
		    i++;
		}
		
		
		return food3List;
	}
 
}
